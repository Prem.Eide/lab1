package rockPaperScissors;

import java.util.Arrays;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

public class RockPaperScissors {

    public Scanner sc = new Scanner(System.in);
    public int roundCounter = 1;
    public int humanScore = 0;
    public int computerScore = 0;
    public String[] rpsChoices = { "rock", "paper", "scissors" };
    public String playerInput = "";
    public String AIInput = "";

    public static void main(String[] args) {
        new RockPaperScissors().run();
    }

    public void run() {
        while (true) {
            System.out.printf("Let's play round %d\n", roundCounter);
            roundCounter += 1;

            playerInput = readInput("Your choice (Rock/Paper/Scissors)?\n");

            AIInput = getAIInput();

            System.out.printf("Human chose %s, computer chose %s. %s\n", playerInput,
                    AIInput,
                    getResult(playerInput, AIInput));
            System.out.printf("Score: human %d, computer %d\n", humanScore,
                    computerScore);

            playerInput = readInput("Do you wish to continue playing? (y/n)?\n");
            if (playerInput.equals("n")) {
                System.out.printf("Bye bye :)");
                break;
            }
        }
    }

    public String readInput(String prompt) {
        System.out.printf(prompt);
        String userInput = sc.next();
        Boolean contains = Arrays.asList(rpsChoices).contains(userInput);

        if (prompt.equals("Do you wish to continue playing? (y/n)?\n")) {
            return userInput;
        }
        while (!contains) {
            System.out.printf("I do not understand %s. Could you try again?\n", userInput);
            userInput = sc.next();
            contains = Arrays.asList(rpsChoices).contains(userInput);
        }
        return userInput;
    }

    public String getAIInput() {
        int randomNum = ThreadLocalRandom.current().nextInt(0, 2 + 1);

        return rpsChoices[randomNum];
    }

    public String getResult(String user, String AI) {
        if (user.equals(AI)) {
            return "It's a tie!";
        }

        if ((user.equals("rock") && AI.equals("scissors"))
                ||
                (user.equals("paper") && AI.equals("rock"))
                ||
                (user.equals("scissors") && AI.equals("paper"))) {
            humanScore += 1;
            return "Human wins!";
        }

        computerScore += 1;
        return "Computer wins!";
    }
}
